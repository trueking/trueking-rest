package com.truekingrest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping(value="/prova")
public class ProvaController {
	
	@RequestMapping(method = RequestMethod.GET)
	public ProvaRest get(HttpServletResponse httpResponse){
		ProvaRest prova = new ProvaRest();
		prova.setId(1337);
		prova.setName("elite");
		return prova;
	}

}
